package adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

public class LogAdapter {
    final static Logger logger = LogManager.getLogger(LogAdapter.class);
    static Gson gson = new Gson();

    public static String logToLog4jException(long startTime, long responseStatus, String urlAPI, String method, String customData, String jsonIn,
	    String jsonOut, String exception) {
	long elapsedTime = System.currentTimeMillis() - startTime;
	return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: 500, URL: " + urlAPI + ", method: GET, " + customData
		+ " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut + ", exception: " + exception;

    }

    public static String logToLog4j(boolean success, long startTime, long responseStatus, String urlAPI, String method, String customData, String jsonIn,
	    String jsonOut) {
	long elapsedTime = System.currentTimeMillis() - startTime;
	if (success) {
	    return "SUCCESS = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + urlAPI + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut;
	} else {
	    return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + urlAPI + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut + "";
	}
    }
    
    public static String logControllerToLog4j(boolean success, long startTime, long responseStatus, String url, String method, String customData,
	    String jsonIn, String jsonOut) {
	long elapsedTime = System.currentTimeMillis() - startTime;

	if (success) {
	    return "SUCCESS = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + url + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut;
	} else {
	    return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + url + ", method: " + method
		    + ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut;
	}
    }

    public static String logControllerToLog4jException(long startTime, long responseStatus, String url, String method, String customData,
	    String jsonIn, String jsonOut, String exception) {
	long elapsedTime = System.currentTimeMillis() - startTime;
	return "FAILED = responseTime: " + elapsedTime + "ms, responseStatus: " + responseStatus + ", URL: " + url + ", method: " + method
		+ ", " + customData + " ~ jsonIn: " + jsonIn + ", jsonOut: " + jsonOut + ", exception: " + exception;
    }
}
