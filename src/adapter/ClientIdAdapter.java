package adapter;

public class ClientIdAdapter {
	public static String getAuthorizationKey(String ClientID, String ClientSecret){
		String stringToSign = ClientID + ":" + ClientSecret;
		String authorizationKey  = Base64Adapter.EncriptBase64(stringToSign); //generate apiKey
		return authorizationKey;
	}
}
