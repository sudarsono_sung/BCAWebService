package model;

import com.google.gson.annotations.SerializedName;

public class mdlErrorSchema {
    @SerializedName(value = "ErrorCode", alternate= {"error-code","error_code"})
    public String ErrorCode;
    @SerializedName(value = "ErrorMessage", alternate= {"error-message","error_message"})
    public model.mdlMessage ErrorMessage;
}
